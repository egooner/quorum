#!/bin/bash
TYPE=$1        # GROUP or INSTANCE
NAME=$2        # Name of the Group or Instance
STATE=$3       # Target state of transition (MASTER, BACKUP or FAULT)

master_detected() {
  echo "Running as Master"
  systemctl start nfsdcld
  systemctl start nfs-server
}

backup_detected() {
  echo "Running as Backup"
  systemctl stop nfs-server
  systemctl stop nfsdcld
}

fault_detected() {
  echo "Running as Fault"
  systemctl stop nfs-server
  systemctl stop nfsdcld
}

case $STATE in 
        "MASTER") master_detected
                  exit 0
                  ;;
        "BACKUP") backup_detected
                  exit 0
                  ;;
        "FAULT")  fault_detected
                  exit 0
                  ;;
        *)        echo "Unknown State - $STATE"
                  exit 1
                  ;;
esac
