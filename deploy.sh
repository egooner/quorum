#!/bin/bash
TARGET=$1
sudo cp config.py $TARGET/
sudo cp mylog.py $TARGET/
sudo cp qclient.py $TARGET/
sudo cp qserver.py $TARGET/
sudo cp vrrpstatechange.sh /usr/local/bin/
sudo cp drbdupdate.sh /usr/local/bin/
