# Todo

## Starting DRBD Service

We could start DRBD automatically on boot up so that we can then validate the resource state

## Normal State

If both boxes are up then we can use simple Quorum test to check reachability and match online state with reachability
However, if both boxes are rebooted 

Box 1 is Primary and reboots
Box 2 will take over
Box 2 reboots
Box 1 will come back up
Box 1 would not have the latest updates and should not be primary

Witness could have some local information on presence to share so that the clients can see who is online

## Power Outage

Box 1 was primary then it is pot luck who comes online 1st
The same condition as Normal state but then the witness may not be available!!
Perhaps each box can share the last state locally

BOX1 : Last Online at ddmmyy hhmmss
BOX2 : Last Online at ddmmyy hhmmss
SERVER: Holds last seen information

BOX1 Sends PING
SERVER Sends all Clients with Last Seen Information for each
If BOX1 sees itself as most recent then it can move to ONLINE
If BOX1 sees alternate as most recent then it moves to OFFLINE for x minutes

