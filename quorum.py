import socket
import struct
import signal
import sys
from threading import Thread, Lock  
from argparse import ArgumentParser, Namespace
import time
import random
from datetime import datetime
import yaml
from os import path, nice, getuid
import subprocess


DEFAULT_MCAST_GRP = "224.1.28.1"
DEFAULT_MCAST_PORT = 6286
DEFAULT_INTERVAL = 1
DEFAULT_RETRY = 3

WITNESS_MSG: str = "WITNESS"
NODE_MSG: str = "NODE"
MSGDELIM = "|"

class Config:
    def __init__(self, args: Namespace) -> None:
        # self.id: str = f"RANDOM{random.random()}"
        self.id: str = socket.gethostname()
        self.configfile: str = None
        self.debug: bool = False
        self.mcastgrp: str = DEFAULT_MCAST_GRP
        self.quorum: list[str] = None
        self.mcastport: int = DEFAULT_MCAST_PORT
        self.msgdelim: str = "|"
        self.interval: int = DEFAULT_INTERVAL
        self.retry: int = DEFAULT_RETRY
        self.notify: str = None
        self.listener: socket = None
        self.sender: socket = None
        self.exit: bool = False
        self.statefile: str = None
        self.bind: str = None
        self.nice: int = 0
        self.witness: bool = False

        kwargs: list[str] = []

        for key, val in args._get_kwargs():
            if hasattr(self, key) and val is not None:
                setattr(self, key, val)
                kwargs.append(key)

        if self.configfile:
            if path.exists(self.configfile) is False:
                log(f"Error - Config File not found - {self.configfile}")
                return
            yamldict: dict = {}
            with open(self.configfile, mode="r", encoding="UTF-8") as yf:
                yamldict = yaml.safe_load(yf)
            for key, val in yamldict.items():
                if key not in kwargs:
                    if hasattr(self, key) and val is not None:
                        setattr(self, key, val)


class Remote:
    def __init__(self, id: str) -> None:
        self.id: str = id
        self.counter: int = 0
        self.online: bool = False
        self.witness: bool = False
        self.lock: Lock = Lock()

remotes: dict[str, Remote] = {}
ignored: list[str] = []
cfg: Config = None

def is_priviliged():
    return getuid() == 0

def log(*args):
    nowstr = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    print(nowstr, *args)

def signal_handler(sig, frame):
    global cfg
    log("Signal Received to close")
    if cfg is not None:
        cfg.exit = True
        cfg.listener.close()
        log("Signal Listener Closed")
    sys.exit(0)


def listen(cfg: Config):
    log(f"Starting Listener for : {cfg.id}")
    cfg.listener = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    cfg.listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    cfg.listener.settimeout(1)
    cfg.listener.bind((cfg.mcastgrp, cfg.mcastport))
    if cfg.bind:
        log(f"Binding to IP Address : {cfg.bind}")
        cfg.listener.setsockopt(socket.IPPROTO_IP,
                                socket.IP_ADD_MEMBERSHIP,
                                socket.inet_aton(cfg.mcastgrp) + socket.inet_aton(cfg.bind))
    else:
        mreq = struct.pack("4sl", socket.inet_aton(cfg.mcastgrp), socket.INADDR_ANY)
        cfg.listener.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
    
    while True:
        try:
            data, addr = cfg.listener.recvfrom(10240)
            receiver(cfg, data, addr)
        except socket.timeout:
            if cfg.exit is False:
                continue
        except:
            if cfg.exit:
                break
    if cfg.debug:
        log("Listener Closing")
        

def receiver(cfg: Config, data: bytes, addr):
    global ignored
    datastr = data.decode("UTF-8")
    client_ip, client_port = addr
    if MSGDELIM in datastr:
        id, _, msg = datastr.partition(cfg.msgdelim)
    else:
        id = None
        msg = datastr

    remote = None
    if id in remotes:
        remote = remotes[id]
    else:
        if cfg.quorum is None:
            remote = Remote(id)
            remotes[id] = remote
        else:
            if id not in ignored:
                ignored.append(id)
                log(f"Ignoring remote ID : {id}, not part of Quorum : {cfg.quorum}")

    if remote is not None:
        if msg not in [WITNESS_MSG, NODE_MSG]:
            log(f"Invalid Message : {msg}, from Node : {id}")
            return

        with remote.lock:
            if remote.counter > cfg.retry:
                log(f"Recovered Id : {id}")                
            remote.counter = 0
            remote.online = True
            remote.witness = True if msg == WITNESS_MSG else False
        if cfg.debug:
            log(f"Received New Message : '{msg}' for Id : '{id}' from : {client_ip}")

def sender(cfg: Config, exitmessage: bool = False):
    message: str = WITNESS_MSG if cfg.witness else NODE_MSG
    if exitmessage is False:
        log(f"Starting Sender Interval : {cfg.interval} with message : {message}")
    msgbytes: bytes = bytes(cfg.id + cfg.msgdelim + message, "UTF-8")
    MULTICAST_TTL = 2
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, MULTICAST_TTL)
    if cfg.bind:
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_IF, socket.inet_aton(cfg.bind))
    if exitmessage:
        msgbytes: bytes = bytes(cfg.id + cfg.msgdelim + "EXIT", "UTF-8")
        sock.sendto(msgbytes, (cfg.mcastgrp, cfg.mcastport))
    else:
        while True:
            if cfg.debug:
                log("Sending ...")
            sock.sendto(msgbytes, (cfg.mcastgrp, cfg.mcastport))
            for i in range(cfg.interval):
                time.sleep(1)
                if cfg.exit:
                    log("Sender Closing, aborting sleep")
                    break
            if cfg.exit:
                log("Sender Closing")
                break

def statechange(cfg: Config, state: bool):
    if cfg.statefile:
        qstate = "OK" if state else "NOK" 
        with open(cfg.statefile, mode="wt", encoding="UTF-8") as qf:
            log(f"Updating file to {state}")
            qf.write(f"{qstate}\n")
        if cfg.notify and path.exists(cfg.notify):
            subprocess.Popen(cfg.notify + " " + qstate, shell=True)
            log(f"Script : {cfg.notify} started")

def checker(cfg: Config):
    log("Starting check routine")
    quorumstate: bool = False
    last_online_count: int = 0
    state_changed: bool = True
    last_nodes_online: list[str] = []
    while True:
        online_count = 0
        nodes_online: list[str] = []
        for id, remote in remotes.items():
            with remote.lock:
                if remote.counter == cfg.retry:
                    log(f"Remote : {id} Counter exceeded max limit")
                    remote.online = False
                    remote.counter += 1
                elif remote.counter < cfg.retry:
                    remote.counter += 1
                if remote.online:
                    online_count += 1
                    if remote.witness is False:
                        nodes_online.append(remote.id)
        remote_count = len(remotes)
        required_for_quorum = int(remote_count / 2) + 1
        nodes_online.sort()
        nodes_online_count = len(nodes_online)
        newnodes = nodes_online == last_nodes_online
        if online_count >= required_for_quorum:
            if quorumstate is False:
                quorumstate = True
                state_changed = True
            elif newnodes:
                state_changed = True
            if state_changed or last_online_count != online_count:
                log(f"Quorum OK, we have {online_count} out of {remote_count}, nodes : {','.join(nodes_online)}")
        else:
            if quorumstate is True:
                quorumstate = False
                state_changed = True
            elif newnodes:
                state_changed = True
            if state_changed or last_online_count != online_count:
                log(f"Quorum FAILED, we have {online_count} out of {remote_count}, nodes : {','.join(nodes_online)}")
        last_online_count = online_count
        last_nodes_online.clear()
        last_nodes_online.extend(nodes_online)
        if state_changed:
            statechange(cfg, quorumstate)
            state_changed = False
        time.sleep(cfg.interval)

def tester():
    hostname = socket.gethostname()
    print(f"HostName : {hostname}")
    addresses = socket.gethostbyname_ex(hostname)
    for addr in addresses:
        print(addr)

def main():
    global cfg, remotes
    parser = ArgumentParser(prog="quorum")
    parser.add_argument("-cfg", dest="configfile", metavar="filename", help="Config File")
    parser.add_argument("-id", dest="id")
    parser.add_argument("-i", dest="interval", type=int)
    parser.add_argument("-m", dest="message")
    parser.add_argument("-q", dest="quorum", nargs="+")
    parser.add_argument("-d", dest="debug", action="store_true")
    parser.add_argument("-w", dest="witness", action="store_true")
    parser.add_argument("-sf", dest="statefile", metavar="filename", help="Quorum State file")
    parser.add_argument("-n", dest="notify", metavar="filename", help="Notify Script to run on state change")
    parser.add_argument("-t", dest="test", action="store_true")
    args = parser.parse_args()

    if args.test:
        tester()
        sys.exit(0)

    cfg = Config(args)


    if cfg.statefile:
        with open(cfg.statefile, mode="w", encoding="UTF-8") as sf:
            sf.write("\n")

    if cfg.quorum:
        for id in cfg.quorum:
            remote = Remote(id)
            remotes[id] = remote

    signal.signal(signal.SIGINT, signal_handler)

    if cfg.nice != 0:
        if is_priviliged():
            current = nice(0)
            adjustment = current + cfg.nice
            log(f"Current NiceNess : {current} New : {cfg.nice} Adjustment : {adjustment}")

            nice(adjustment)
            log(f"New Niceness : {nice(0)}")
        else:
            log(f"Unable to set niceness to {cfg.nice} as running unpriviliged")

    t_recv = Thread(target=listen, args=(cfg,))
    t_recv.start()
    t_send = Thread(target=sender, args=(cfg,))
    t_send.start()

    time.sleep(cfg.interval + 1)
    checker(cfg)

    t_recv.join()
    log("Listener Closed")
    t_send.join()
    log("Receiver Closed")

if __name__ == "__main__":
    main()



