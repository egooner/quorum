import socket
import sys
from argparse import ArgumentParser
from config import Config, Remote
from mylog import log, err, msg, logtosyslog

cfg: Config = None

# def signal_handler(sig, frame):
#     global cfg
#     log("Signal Received to close")
#     if cfg is not None:
#         cfg.exit = True
#         cfg.listener.close()
#         log("Signal Listener Closed")
#     sys.exit(0)


def listen(cfg: Config):
    '''
    listen - Start listening for incoming messages from qclients

    Parms
    -----
    cfg: Config - Configuration Data

    Returns
    -------
    None
    '''
    if cfg.id not in cfg.nodes:
        err(f"Error : No IP mapping found for Id : {cfg.id}")
        return
    bindto: str = cfg.nodes[cfg.id].addr
    msg(f"Starting Listener for : {cfg.id} on : {bindto}:{cfg.serverport}")
    listener = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listener.bind((bindto, cfg.serverport))
    while True:
        try:
            data, addr = listener.recvfrom(10240)
            receiver(cfg, data, addr)
        except:
            msg("Terminating Listen Loop!")
            break
    if cfg.debug:
        log("Listener Closing")
        

def receiver(cfg: Config, data: bytes, addr):
    '''
    receiver - Handle data messages from listener

    Parms
    -----
    cfg: Config - Configuration Data
    data: bytes - Received data message
    addr: tuple - IP Address and Port of client

    Returns
    -------
    None
    '''
    remotedata = data.decode("UTF-8")
    if remotedata is None:
        remotedata = ""
    client_ip, _ = addr
    if client_ip not in cfg.ipmap:
        err(f"Error : Remote IP not found in IP MAP - {client_ip}")
        return
    remote: Remote = cfg.ipmap[client_ip]
    if len(remotedata) < 4 or remotedata[0:4] != "PING":
        err(f"Error : Invalid Data Received from : {remote.name} IP : {client_ip}")
        return
    clientport = cfg.clientport
    if len(remotedata) > 4 and remotedata[4] == ":":
        try:
            clientport = int(remotedata[5:])
        except:
            err(f"Error : Ping got non numeric Port : {remotedata[5:]}")
        send_reponse(cfg, remote, client_ip, clientport)

def send_reponse(cfg: Config, remote: Remote, addr_to: str, port_to: int):
    log(f"Sending Reponse to : {remote.name} IP : {addr_to}:{port_to}")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    sock.sendto(bytes("PONG", "UTF-8"), (addr_to, port_to))

def main():
    global cfg, remotes
    parser = ArgumentParser(prog="quorum")
    parser.add_argument("-cfg", dest="configfile", metavar="filename", help="Config File")
    parser.add_argument("-id", dest="id")
    parser.add_argument("-d", dest="debug", action="store_true")
    parser.add_argument("-q", dest="quiet", action="store_true")
    parser.add_argument("-t", dest="testmode", action="store_true")
    args = parser.parse_args()

    if args.testmode is False:
        logtosyslog("qserver")

    cfg = Config(args)

    # signal.signal(signal.SIGINT, signal_handler)

    listen(cfg)

if __name__ == "__main__":
    main()



