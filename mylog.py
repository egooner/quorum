from datetime import datetime
import logging
from logging.handlers import SysLogHandler

QUITEMODE: bool = False
USELOGGER: bool = False
LOGGER = None

def setquiet(state: bool):
    global QUITEMODE
    QUITEMODE = state

def logtosyslog(name: str, addhandler: bool = False):
    global USELOGGER, LOGGER
    USELOGGER = True
    if USELOGGER:
        LOGGER = logging.getLogger(name=name)
        if addhandler:
            formatter = logging.Formatter(
                fmt="%(name)s: %(message)s"
            )
            handler = SysLogHandler(
                facility=SysLogHandler.LOG_DAEMON, address="/dev/log"
            )
            handler.setFormatter(formatter)
            LOGGER.setLevel(logging.INFO)
            LOGGER.addHandler(handler)
        else:
            logging.basicConfig(encoding="UTF-8", level=logging.INFO, format="%(name)s: %(message)s")


def log(*args):
    if QUITEMODE is False:
        if USELOGGER:
            LOGGER.info(" ".join(map(str, args)))
        else:
            nowstr = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
            print(nowstr, *args)


def msg(*args):
    if USELOGGER:
        LOGGER.info(" ".join(map(str, args)))
    else:
        nowstr = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        print(nowstr, *args)


def err(*args):
    if USELOGGER:
        LOGGER.error(" ".join(map(str, args)))
    else:
        nowstr = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        print(nowstr, *args)
