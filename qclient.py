import socket
from threading import Thread  
from argparse import ArgumentParser, RawTextHelpFormatter
import time
from config import Config, Remote
from mylog import log, err, logtosyslog
import sys
try:
    from sh import drbdadm
except ImportError:
    print("Running without DRBDADM")
from subprocess import run, PIPE
from os import path, remove

STATE_QUORUM = 0
STATE_NOQUORUM = 1
STATE_TOOSOON = 2
STATE_DRBD_PRIMARY_FAILED = 4
STATE_DRBD_SECONDARY_FAILED = 8
STATE_OVERRIDE_OK = 16
STATE_OVERRIDE_FAILED = 32
STATE_FAILOVER_OK = 64
STATE_FAILOVER_FAILED = 128
STATE_FAILOVER_ABORTED = 256
FORCE_UP = 1
FORCE_DOWN = 0


class DRBD:
    def __init__(self,
                 resource: str,
                 script: str,
                 onprimary: list[str],
                 onsecondary: list[str]
                 ) -> None:
        self.name: str = resource
        self.script: str = script
        self.onprimary: list[str] = onprimary
        self.onsecondary: list[str] = onsecondary

    @property
    def isvalid(self) -> bool:
        return self.name is not None

    def get_roles(self):
        if self.isvalid is False:
            return None, None

        role, _, r_role = str(drbdadm("role", self.name)).strip().partition("/")
        return role.lower(), r_role.lower()
    
    def get_role(self) -> str:
        return self.get_roles()[0]
    
    def get_remote_role(self) -> str:
        return self.get_roles()[1]
    
    def get_connection(self):
        if self.isvalid is False:
            return None

        return str(drbdadm("cstate", self.name)).strip().lower()
    
    def get_disk_states(self):
        if self.isvalid is False:
            return None, None

        disk, _, r_disk = str(drbdadm("dstate", self.name)).strip().partition("/")
        return disk.lower(), r_disk.lower()
    
    def get_disk_state(self):
        return self.get_disk_states()[0]
    
    def get_remote_disk_state(self):
        return self.get_disk_states()[1]

    def set_primary_role(self):
        role, r_role = self.get_roles()
        disk, r_disk = self.get_disk_states()
        conn = self.get_connection()
        if role == "primary" and disk == "uptodate":
            log(f"DRBD Resource {self.name} already primary and up to date")
            return True
        if role == "secondary" and r_role != "primary" and disk == "uptodate":
            self.set_drbd_state("primary")
            return True
        return False

    def set_secondary_role(self):
        role, r_role = self.get_roles()
        if role is not None and role != "secondary":
            self.set_drbd_state("secondary")
        else:
            log(f"DRBD Resource {self.name} has state {role}")
        return True

    def set_drbd_state(self, newstate: str):
        if self.isvalid is False:
            return
        log(f"Updating DRBD Resource : {self.name} to newstate : {newstate}")
        if self.script is not None:
            log(f"Running Script : {self.script}")
            result = run([self.script, self.name, newstate],
                         stdout=PIPE, stderr=PIPE, text=True, shell=True)
            if result is not None:
                log(f"DRBD State Script Returned : {result.returncode}")
                if result.stdout is not None and result.stdout != "":
                    log(f"StdOut : {result.stdout}")
                if result.stderr is not None and result.stderr != "":
                    log(f"StdErr : {result.stderr}")
            else:
                log("Script Returned None!")
        elif newstate == "primary" and self.onprimary:
            drbdadm(newstate, self.name)
            for cmd in self.onprimary:
                result = run(cmd, shell=True, capture_output=True, text=True)
                if result.stdout != "" and result.stderr != "":
                    textout = result.stdout + "\n" + result.stderr
                elif result.stdout != "":
                    textout = result.stdout
                elif result.stderr != "":
                    textout = result.stderr
                else:
                    textout = ""
                log(f"Cmd : {cmd} Code : {result.returncode}")
                if textout != "":
                    for line in textout.split("\n"):
                        log(line)
        elif newstate == "secondary" and self.onsecondary:
            for cmd in self.onsecondary:
                result = run([cmd], shell=True, capture_output=True, text=True)
                if result.stdout != "" and result.stderr != "":
                    textout = result.stdout + "\n" + result.stderr
                elif result.stdout != "":
                    textout = result.stdout
                elif result.stderr != "":
                    textout = result.stderr
                else:
                    textout = ""
                log(f"Cmd : {cmd} Code : {result.returncode}")
                if textout != "":
                    for line in textout.split("\n"):
                        log(line)
            drbdadm(newstate, self.name)
        else:
            drbdadm(newstate, self.name)


def get_uptime():
    '''
    get_uptime - Get number of seconds since boot up
    '''
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])

    return uptime_seconds


def probe(cfg: Config) -> bool:
    '''
    probe - Start a probe to check if we have quorum

    Parms
    -----
    cfg: Config - Configuration data

    Returns
    -------
    bool - True/False for quorum state
    '''
    if cfg.id not in cfg.nodes:
        err(f"Error : No IP mapping found for Id : {cfg.id}")
        return
    
    remotenames: list[str] = []
    for remotename in cfg.nodes.keys():
        if remotename != cfg.id or cfg.loopback:
            remotenames.append(remotename)

    remotenames.sort()

    localnode: Remote = cfg.nodes[cfg.id]

    log(f"Starting Listener for Probe {len(remotenames)} Responses to {cfg.id} at : {localnode.addr}:{cfg.clientport}")
    listener = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listener.settimeout(cfg.sockettimeout)

    listener.bind((localnode.addr, cfg.clientport))

    required: int = len(remotenames)
    quorum: int = int((len(remotenames) + 1) / 2)
    received: int = 0
    log(f"Quorum requires at least : {quorum} response")
    for _ in range(cfg.retries):
        start_time: float = time.time()
        task = Thread(target=send_messages, args=(cfg, remotenames))
        task.start()
        while True:
            try:
                data, addr = listener.recvfrom(10240)
                remotename: str = receiver(cfg, data, addr)
                if remotename is not None and remotename in remotenames:
                    remotenames.remove(remotename)
                    required -= 1
                    received += 1
                    if required == 0:
                        break
            except socket.timeout:
                newtime: float = time.time()
                elapsed = newtime - start_time
                if elapsed >= cfg.sockettimeout and received >= quorum:
                    break
                if elapsed >= cfg.waittime:
                    break
                continue
            except:
                log("Terminating Listen Loop!")
                break
        task.join()
        if required == 0:
            break
        if received >= quorum:
            break
    if required > 0:
        log(f"Warning : No response from {required} nodes : {','.join(remotenames)}")
    if cfg.debug:
        log("Listener Closing")
    log("Send and Listen all done!")
    return received >= quorum


def receiver(cfg: Config, data: bytes, addr):
    '''
    receiver - Handles data received by the probe function

    Parms
    -----
    cfg: Config - Configuration Data
    data: bytes - Data
    addr: tuple - IP address and Port Number

    Returns
    -------
    str - Remote name or None
    '''
    remotedata = data.decode("UTF-8")
    client_ip, _ = addr
    if client_ip not in cfg.ipmap:
        err(f"Error : Remote IP not found in IP MAP - {client_ip}")
        return None

    remote: Remote = cfg.ipmap[client_ip]
    if remotedata != "PONG":
        err(f"Error : Invalid Response received from : {remote.name} IP : {client_ip}")
        return None

    with remote.lock:
        remote.resptime = time.time()
        log(f"Response Received from : {remote.name} in {remote.resptime - remote.sendtime:.6f}s")    
    return remote.name


def send_messages(cfg: Config, remotenames: list[str]):
    '''
    send_messages - Runs as background thread to send messages to the QServer

    Parms
    -----
    cfg: Config - Configuration Data
    remotes: list[str] - Names of remotes to attempt to connect to

    Returns
    -------
    None
    '''
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    for remotename in remotenames:
        remote: Remote = cfg.nodes[remotename]
        with remote.lock:
            log(f"Sending PING to : {remote.name} - {remote.addr}:{cfg.serverport}")
            remote.sendtime = time.time()
            sock.sendto(
                bytes(f"PING:{cfg.clientport}", "UTF-8"),
                (remote.addr, cfg.serverport)
            )

def terminate(code: int, debug: bool):
    if debug:
        log(f"Terminating With Code : {code}")
    sys.exit(code)


def main():
    '''
    Main Task
    '''
    parser = ArgumentParser(prog="quorum", formatter_class=RawTextHelpFormatter)
    parser.add_argument("-cfg", dest="configfile", metavar="filename", help="Config File")
    parser.add_argument("-id", dest="id", help="Identifier for the client, defaults to hostname")
    parser.add_argument("-d", dest="debug", action="store_true", help="Enable debugging")
    parser.add_argument("-l", dest="loopback", action="store_true", help="Enable ping to self \"Loopback\"")
    parser.add_argument("-s", dest="logtosyslog", action="store_true", help="Send log messages to the syslog")
    parser.add_argument("-fd", dest="forcedown", action="store_true", help="Set the override file to force down")
    parser.add_argument("-fu", dest="forceup", action="store_true", help="Set the override file to force up")
    parser.add_argument("-fn", dest="forceneutral", action="store_true", help="Reset the override file to force neutral")
    parser.add_argument("-fo", dest="failover", action="store_true", help="Failover from primary to secondary")
    parser.add_argument("-q", dest="quiet", action="store_true", help="Quiet mode, do not display responses")
    codes = [
        f"{STATE_QUORUM} - Quorum OK",
        f"{STATE_NOQUORUM} - Quorum Not OK",
        f"{STATE_TOOSOON} - Quorum OK but too soon after reboot see uptime",
        f"{STATE_DRBD_PRIMARY_FAILED} - Quorum OK and failed to set Primary",
        f"{STATE_DRBD_SECONDARY_FAILED} - Quorum OK and failed to set Secondary"
    ]
    t = Config(None)
    # for attr, value in t.__dict__.items():
    #     if attr.__doc__ is not None:
    #         print(f"{attr} {attr.__doc__}")
    #     else:
    #         print(f"{attr}")
    parser.epilog = "\n".join(codes)
    args = parser.parse_args()

    cfg = Config(args)
    if cfg.logtosyslog:
        log("Activating Syslog for QClient")
        logtosyslog("qclient", True)

    override: bool = cfg.forcedown or cfg.forceup or cfg.forceneutral or cfg.failover
    if cfg.override and override:
        try:
            if cfg.forcedown:
                with open(cfg.override, mode="wt", encoding="UTF-8") as outfile:
                    outfile.write(str(FORCE_DOWN))
            elif cfg.forceup:
                with open(cfg.override, mode="wt", encoding="UTF-8") as outfile:
                    outfile.write(str(FORCE_UP))
            elif cfg.forceneutral and path.exists(cfg.override):
                remove(cfg.override)
            elif cfg.failover:
                drbd = DRBD(cfg.drbdresource, cfg.drbdscript, cfg.onprimary, cfg.onsecondary)
                localrole: str = drbd.get_role()
                if localrole != "primary":
                    log(f"Aborting failover as local system is not primary")
                    terminate(STATE_FAILOVER_ABORTED, cfg.debug)
                with open(cfg.override, mode="wt", encoding="UTF-8") as outfile:
                    outfile.write(str(FORCE_DOWN))
                tcode = STATE_FAILOVER_OK
                for _ in range(60):
                    remoterole = drbd.get_remote_role()
                    if remoterole == "primary":
                        print("")
                        log("Failover to remote system OK")
                        break
                    print(".", end="", flush=True)
                    time.sleep(1)
                else:
                    print("")
                    log(f"Failover to remote system failed!!")
                    tcode = STATE_FAILOVER_FAILED
                remove(cfg.override)
                terminate(tcode, cfg.debug)


            terminate(STATE_OVERRIDE_OK, cfg.debug)
        except Exception as e:
            log(f"Error Updating Override {cfg.override} - {e}")
            terminate(STATE_OVERRIDE_FAILED, cfg.debug)

    if cfg.override and path.exists(cfg.override):
        with open(cfg.override, mode="rt", encoding="UTF-8") as infile:
            overriden: str = infile.read(1)
            quorum = overriden == str(FORCE_UP)
        log(f"Forced Quorum State : {quorum}")
    else:
        quorum = probe(cfg)
        log(f"Quorum State : {quorum}")

    drbd = DRBD(cfg.drbdresource, cfg.drbdscript, cfg.onprimary, cfg.onsecondary)

    if quorum:
        if cfg.uptime > 0:
            uptime = get_uptime()
            if uptime < cfg.uptime:
                terminate(STATE_TOOSOON, cfg.debug)
        if drbd.set_primary_role() is False:
            terminate(STATE_DRBD_PRIMARY_FAILED, cfg.debug)
    else:
        if drbd.set_secondary_role() is False:
            terminate(STATE_DRBD_SECONDARY_FAILED, cfg.debug)

    terminate(STATE_QUORUM if quorum else STATE_NOQUORUM, cfg.debug)


if __name__ == "__main__":
    main()
