#!/bin/bash
RESOURCE=$1
STATE=$2
DRBDADMIN=/usr/sbin/drbdadm
UMOUNT=/usr/bin/umount
MOUNT=/usr/bin/mount
DISK=${RESOURCE:1:1}

if [ "$STATE" = "primary" ]; then
  $DRBDADMIN primary $RESOURCE
  sleep 2
  $MOUNT /dev/drbd$DISK /srv/$RESOURCE
fi
if [ "$STATE" = "secondary" ]; then
  systemctl stop nfs-server
  if [ $? -eq 0 ]; then
    echo "NFS Server Stopped"
  else
    echo "NFS Server Stop Failed!"
  fi
  sleep 1
  systemctl stop nfsdcld
  if [ $? -eq 0 ]; then
    echo "NFS4 State Service nfsdlcd stopped"
  else
    echo "NFS4 State Service nfsdcld stop failed"
  fi
  sleep 1
  $UMOUNT /dev/drbd$DISK
  if [ $? -eq 0 ]; then
    echo "Disk $DISK Unmount for $RESOURCE Complete"
  else
    echo "Disk $DISK Unmount for $RESOURCE Failed"
  fi
  sleep 1
  $DRBDADMIN secondary $RESOURCE
  if [ $? -eq 0 ]; then
    echo "DRBD Role for $RESOURCE set to secondary"
  else
    echo "DRBD Role for $RESOURCE failed setting to secondary"
  fi
fi