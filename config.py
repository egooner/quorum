from argparse import Namespace
from socket import gethostname
import yaml
from os import path
from mylog import log, setquiet
from threading import Lock

class Remote:
    def __init__(self, name: str, addr: str):
        self.name: str = name
        self.addr: str = addr
        self.sendtime: float = 0
        self.resptime: float = 0
        self.lock: Lock = Lock()


class Config:
    def __init__(self, args: Namespace) -> None:
        self.id: str = gethostname()
        """ HostName or provided identifier """
        self.configfile: str = None
        """ Configuration File Name """
        self.debug: bool = False
        self.quiet: bool = False
        self.loopback: bool = False
        self.sockettimeout: float = 0.1
        """ Socket timeout in seconds """
        self.waittime: float = 1.0
        """ How long to wait before doing a retry """
        self.retries: int = 3
        """ Number of retries of waittime """
        self.uptime: float = 0.0
        """ Number of seconds system must have been up for to report active """
        self.serverport: int = 6289
        """ Server Port for QServer to Listen for requests on """
        self.clientport: int = 6270
        """ Client Port for QClient to Listen for responses on """
        self.drbdresource: str = None
        """ DRBD resource to monitor"""
        self.drbdscript: str = None
        """ Script location and name to call to change state p1 = resource, p2 = primary/secondary"""
        self.onprimary: list[str] = None
        """ List of commands to run when primary """
        self.onsecondary: list[str] = None
        """ List of commands to run when secondary """
        self.override: str = None
        """ Override state using a override file, file will contain a code """
        self.forcedown: bool = False
        """ Set the force down flag to write to the override file """
        self.forceup: bool = False
        """ Set the force up flag to write to the override file """
        self.forceneutral: bool = False
        """ Reset the force state of the override file """
        self.failover: bool = False
        """ Perform a failover """
        self.nodes: dict[str, Remote] = {}
        """ Dictionary of Node Names and their IP Addresses """
        self.logtosyslog: bool = False
        """ Force logging to the Syslog on the local system """
        self.ipmap: dict[str, Remote] = {}
        """ Dictionary of IP Addresses and their Node Names calculated from nodes """

        kwargs: list[str] = []

        if args is not None:
            for key, val in args._get_kwargs():
                if key == "nodes":
                    for name, addr in val.items():
                        self.nodes[name] = Remote(name, addr)
                        self.ipmap[addr] = self.nodes[name]
                elif isinstance(val, bool) and val is False:
                    pass
                elif hasattr(self, key) and val is not None:
                    setattr(self, key, val)
                    kwargs.append(key)

            if self.configfile:
                if path.exists(self.configfile) is False:
                    log(f"Error - Config File not found - {self.configfile}")
                    return
                yamldict: dict = {}
                with open(self.configfile, mode="r", encoding="UTF-8") as yf:
                    yamldict = yaml.safe_load(yf)
                for key, val in yamldict.items():
                    if key not in kwargs:
                        if key == "nodes":
                            for name, addr in val.items():
                                self.nodes[name] = Remote(name, addr)
                                self.ipmap[addr] = self.nodes[name]
                        elif hasattr(self, key) and val is not None:
                            setattr(self, key, val)

            setquiet(self.quiet)
